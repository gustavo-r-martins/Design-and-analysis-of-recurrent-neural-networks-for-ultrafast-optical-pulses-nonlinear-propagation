# Design and analysis of recurrent neural networks for ultrafast optical pulses nonlinear propagation (2022)

## Folders
### data
This folder contains the temporal and spectral data made in Matlab.

### models
This folder contains the Colab notebooks used to train and to test the recurrent neural networks.

### nets
This folder contains the best recurrent neural networks [weights] found for the nonlinear propagation problem.

### results
This folder summarizes the best results found, it contains details of the networks build and their metrics.
